Feature: Edumate API Me

  @me

  Scenario Outline: positive me with bear token
    Given scenario name "positive me with bear token"
    Given bluevalley request url "/api/authorize"
    Then authorize post request with body client Id <clientId> , client secret <clientSecret> , refresh token <refreshToken>
    Then validate bear token
    Given bluevalley request url "/api/me"
    Then me get request with valid access token
    Then validate success as true
    Then validate me data response with <contactId> and <firstname> and <surname> and <emailAddress>

    Examples:
      | user | clientId  | clientSecret | refreshToken | contactId | firstname | surname | emailAddress |
      | positive_carer_jarad_1_with_token_1 | 06182b03689d6fb842752926561cb256 | f444a23b959ffcaf3993a3e34ee16de953d4fe0cb50804e655539e2ebe0b3e37 | 1 | 2340 | Jarad | Boroughs | joyce.gao@gmail.com |
      | positive_carer_jarad_2_with_token_1 | 06182b03689d6fb842752926561cb256 | 06c77b7f17757bd2f9f5b68952ec75d44938dadcb8ccc5b77e11adc142b35fba | 1 | 2340 | Jarad | Boroughs | joyce.gao@gmail.com |
      | positive_admin_with_token_1         | d0f90423660b1bc1937ad8688f192e65 | 3fce8fdf91b073261ef7e1a25c42ffdbc3f3db8c08c261574d222ae28dc12a62 | 1 | 1 | School | Admin | jossandro@gmail.com |
      | positive_student_with_token_1       | d270be60b18f77792ca984a3625e7fdd | c148806cc6170f79798cb00dc2377d1858824145fb6c1646ad10241b71b66ec3 | 1 | 531 | Kyan | Boroughs | joyce.gao@edumate.com.au |

  Scenario Outline: negative me with invalid bear token
    Given scenario name "negative me with invalid bear token"
    Given bluevalley request url "/api/authorize"
    Then authorize post request with body client Id <clientId> , client secret <clientSecret> , refresh token <refreshToken>
    Then validate bear token
    Given bluevalley request url "/api/me"
    Then <api> get request with invalid access token
    Then validate success as false
    Then validate data error message of invalid token

    Examples:
      | api | user | clientId  | clientSecret | refreshToken |
      | ME | negative_carer_jarad_1_with_token_1 | 06182b03689d6fb842752926561cb256 | f444a23b959ffcaf3993a3e34ee16de953d4fe0cb50804e655539e2ebe0b3e37 | 1 |
      | ME | negative_carer_jarad_2_with_token_1 | 06182b03689d6fb842752926561cb256 | 06c77b7f17757bd2f9f5b68952ec75d44938dadcb8ccc5b77e11adc142b35fba | 1 |
      | ME | negative_admin_token_1 | d0f90423660b1bc1937ad8688f192e65 | 3fce8fdf91b073261ef7e1a25c42ffdbc3f3db8c08c261574d222ae28dc12a62 | 1 |
      | ME | negative_student_token_1 | d270be60b18f77792ca984a3625e7fdd | c148806cc6170f79798cb00dc2377d1858824145fb6c1646ad10241b71b66ec3 | 1 |