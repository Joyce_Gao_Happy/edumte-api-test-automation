Feature: Broadcast feaure

  @broadcast
  Scenario Outline: Broadcast list
    Given scenario name "Broadcast list"
    Given username "jarad" for the login request "/demo/edu5/web/app.php/rest/api/getToken"
    Then verify request generates token
    Given uri "/demo/edu5/web/app.php/rest/api/broadcasts?"
    When startdate <startdate> , enddate <enddate> , limit <limit> and offset <offset>
    Then validate the status code "200"
    Then validate the photos count

    Examples:
      | startdate  | enddate    | limit | offset |
      | 2019-06-30 | 2019-12-01 | 1     | 2      |
      | 2019-06-30 | 2019-07-30 | 1     | 2      |