Feature: Broadcast and Absence management feaure

  @absent
  Scenario Outline: Mark absent
    Given scenario name "Mark absent"
    Given username "tfreeman" for the login request "/demo/edu5/web/app.php/rest/api/getToken"
    Then verify request generates token
    Given uri "/demo/edu5/web/app.php/rest/api/absences/explanations"
    Then mark absent for the studentid <studentid> , absencereasonId <absenceReasonId> , abscencedetails <absenceDetails> and info <info>
    Then validate the status code "200"
    Then validate the absent record

    Examples:
      | studentid  | absenceReasonId    | absenceDetails | info |
      | 572 | 88 | fever     | 2019-08-29,__New__      |
      | 872 | 88 | fever     | 2019-08-29,__New__      |