Feature: Edumate API Authorize

  @authorize

  Scenario Outline: positive authorize with client_id and client_secret and refresh_token
    Given scenario name "positive authorize with client_id, client_secret and refresh_token"
    Given bluevalley request url "/api/authorize"
    Then authorize post request with body client Id <clientId> , client secret <clientSecret> , refresh token <refreshToken>
    Then validate success as true
    Then validate data keys

    Examples:
      | user | clientId  | clientSecret | refreshToken |
      | positive_carer_jarad_1_with_token_1 | 06182b03689d6fb842752926561cb256 | f444a23b959ffcaf3993a3e34ee16de953d4fe0cb50804e655539e2ebe0b3e37 | 1 |
      | positive_carer_jarad_2_with_token_1 | 06182b03689d6fb842752926561cb256 | 06c77b7f17757bd2f9f5b68952ec75d44938dadcb8ccc5b77e11adc142b35fba | 1 |
      | positive_admin_token_1 | d0f90423660b1bc1937ad8688f192e65 | 3fce8fdf91b073261ef7e1a25c42ffdbc3f3db8c08c261574d222ae28dc12a62 | 1 |
      | positive_student_token_1 | d270be60b18f77792ca984a3625e7fdd | c148806cc6170f79798cb00dc2377d1858824145fb6c1646ad10241b71b66ec3 | 1 |
      | positive_carer_jarad_1_with_token_0 | 06182b03689d6fb842752926561cb256 | f444a23b959ffcaf3993a3e34ee16de953d4fe0cb50804e655539e2ebe0b3e37 | 0 |
      | positive_carer_jarad_2_with_token_0 | 06182b03689d6fb842752926561cb256 | 06c77b7f17757bd2f9f5b68952ec75d44938dadcb8ccc5b77e11adc142b35fba | 0 |
      | positive_admin_with_token_0 | d0f90423660b1bc1937ad8688f192e65 | 3fce8fdf91b073261ef7e1a25c42ffdbc3f3db8c08c261574d222ae28dc12a62 | 0 |
      | positive_student_with_token_0 | d270be60b18f77792ca984a3625e7fdd | c148806cc6170f79798cb00dc2377d1858824145fb6c1646ad10241b71b66ec3 | 0 |


  # Edumate API: Positive Authentication API with client_id, client _secret, without refresh_token
  # https://edumate.atlassian.net/browse/TES-1250
  Scenario Outline: positive authorize with client_id and client_secret
    Given scenario name "positive authorize with client_id, client_secret"
    Given bluevalley request url "/api/authorize"
    Then authorize post request with client Id <clientId> , client secret <clientSecret>
    Then validate success as true
    Then validate data keys

    Examples:
      | user | clientId  | clientSecret |
      | positive_carer_jarad_1_without_token| 06182b03689d6fb842752926561cb256 | f444a23b959ffcaf3993a3e34ee16de953d4fe0cb50804e655539e2ebe0b3e37 |
      | positive_carer_jarad_2_without_token | 06182b03689d6fb842752926561cb256 | 06c77b7f17757bd2f9f5b68952ec75d44938dadcb8ccc5b77e11adc142b35fba |
      | positive_admin_without_token | d0f90423660b1bc1937ad8688f192e65 | 3fce8fdf91b073261ef7e1a25c42ffdbc3f3db8c08c261574d222ae28dc12a62 |
      | positive_student_without_token | d270be60b18f77792ca984a3625e7fdd | c148806cc6170f79798cb00dc2377d1858824145fb6c1646ad10241b71b66ec3x |


  Scenario Outline: negative authorize without client_id or client_secret
    Given scenario name "Negative authorize without client_id or client_secret"
    Given bluevalley request url "/api/authorize"
    Then authorize post request without client Id or client secret <clientArgument>
    Then validate success as false
    Then validate data error message of missing client id  or client secret

    Examples:
      | user | clientArgument |
      | negative_carer_jarad_1_client_secret | be4e6ffaae05d61dd5a26074693e1f90e2a6358c6ede07bdd652c5d23341bfe4 |
      | negative_carer_jarad_2_client_secret | e8dc801c0281583cd7b5259adbce26766cb61756951ee7953a7c39ae2252f499 |
      | negative_admin_client_secret | 1fe9324990a878201bb0c7b59b92e113a69c891610131ab437728b5610c1b563 |
      | negative_student_client_secret | 3ad24ce6d96b49d4a153e05e5f0115a2c213d2b66684f0ddb8f1a3e2ea926bcd |
      | negative_carer_jarad_1_client_id |  e70723f84911dc893773e73839a69c47  |
      | negative_carer_jarad_2_client_id |  e70723f84911dc893773e73839a69c47 |
      | negative_admin_client_id | d0f90423660b1bc1937ad8688f192e65 |
      | negative_student_client_id | 8ea96fe0768af571249d8b158f6e768d  |


  Scenario Outline: negative authorize with invalid client_id or client_secret
    Given scenario name "Negative authorize with invalid client_id or client_secret"
    Given bluevalley request url "/api/authorize"
    Then authorize post request body with invalid client Id <clientId> or client secret <clientSecret>
    Then validate success as false
    Then validate data error message of invalid client id or client secret

    Examples:
      | user | clientId  | clientSecret |
      | negative_carer_jarad_1_with_invalid_client_id| 12345678900987654321 |8ea96fe0768af571249d8b158f6e768d|
      | negative_carer_jarad_2_invalid_client_id| invalid_client_id |e8dc801c0281583cd7b5259adbce26766cb61756951ee7953a7c39ae2252f499|
      | negative_admin_invalid_client_id| !@#~%^&*()_+}:"<>? |1fe9324990a878201bb0c7b59b92e113a69c891610131ab437728b5610c1b563|
      | negative_student_invalid_client_id| aws1234()_+:"<>?P{:?>wsw |3ad24ce6d96b49d4a153e05e5f0115a2c213d2b66684f0ddb8f1a3e2ea926bcd|
      | negative_carer_jarad_1_with_invalid_client_secret|e70723f84911dc893773e73839a69c47| 12345678900987654321 |
      | negative_carer_jarad_2_with_invalid_client_secret |e70723f84911dc893773e73839a69c47| invalid_client_id |
      | negative_admin_with_invalid_client_secret |d0f90423660b1bc1937ad8688f192e65| !@#~%^&*()_+}:"<>? |
      | negative_student_with_invalid_client_secret |8ea96fe0768af571249d8b158f6e768d| aws1234()_+:"<>?P{:?>wsw!@#$%  |
      | positive_student_with_token_0_test_fail | 8ea96fe0768af571249d8b158f6e768d | 3ad24ce6d96b49d4a153e05e5f0115a2c213d2b66684f0ddb8f1a3e2ea926bcd |


  Scenario Outline: negative authorize with invalid URL
    Given scenario name "negative authorize with invalid URL"
    Given bluevalley request url "/api/authorize/invalid"
    Then authorize post request with body client Id <clientId> , client secret <clientSecret> , refresh token <refreshToken>
    Then validate success as false
    Then validate data error message of resource not found

    Examples:
      | user | clientId  | clientSecret | refreshToken |
      | negative_carer_jarad_1_with_token_1 | e70723f84911dc893773e73839a69c47 | be4e6ffaae05d61dd5a26074693e1f90e2a6358c6ede07bdd652c5d23341bfe4 | 1 |
      | negative_carer_jarad_2_with_token_1 | e70723f84911dc893773e73839a69c47 | e8dc801c0281583cd7b5259adbce26766cb61756951ee7953a7c39ae2252f499 | 1 |
      | negative_admin_token_1 | d0f90423660b1bc1937ad8688f192e65 | 1fe9324990a878201bb0c7b59b92e113a69c891610131ab437728b5610c1b563 | 1 |
      | negative_student_token_1 | 8ea96fe0768af571249d8b158f6e768d | 3ad24ce6d96b49d4a153e05e5f0115a2c213d2b66684f0ddb8f1a3e2ea926bcd | 1 |