<?php

use Behat\Behat\Context\Context;

class EdumateAPIAdminRoutesSteps implements Context
{
    /**
     * @Then /^admin routers get request with valid access token$/
     */
    public function adminRoutersGetRequestWithValidAccessToken()
    {
        $logger = Logger::getLogger("API");
        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $bear_token"
                    ]
                ]
            );
            $common::$response = $common::$response->getBody()->read(1024);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

    /**
     * @Then /^validate response lms$/
     */
    public function validateResponseLms()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ADMIN ROUTES API RESPONSE DATA VALIDATION");
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        if(array_key_exists('lms',$data)==true){
            #$dataData = $data['data'];
            $logger->info("[Record] - response body data detail: " . json_encode($data));
            $lms = $data['lms'];
            // validation
            PHPUnit\Framework\Assert::assertEquals(["list_teachers", "student_formruns"], $lms);
            $logger->info("[PASS] - lms validation is passed");
        }else{
            $logger->info("[PASS] - no lms date");
        }
    }
}