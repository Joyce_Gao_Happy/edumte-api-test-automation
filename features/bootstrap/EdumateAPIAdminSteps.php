<?php

use Behat\Behat\Context\Context;

class EdumateAPIAdminSteps implements Context
{
    /**
     * @Then /^validate admin data response with (.*) and (.*)$/
     */
    public function validateAdminDataResponseWithAnd($adminCall, $adminRoutes)
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ADMIN API RESPONSE DATA VALIDATION");
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        #$dataData = $data['data'];
        $logger->info("[Record] - response body data detail: " . json_encode($data));
        $adminCallValue = $data['/admin/calls'];
        $adminRoutesValues = $data['/admin/routes'];
        // validation
        PHPUnit\Framework\Assert::assertEquals($adminCallValue, $adminCall);
        $logger->info("[Pass] - admin/call validation is passed");
        PHPUnit\Framework\Assert::assertEquals($adminRoutesValues, $adminRoutes);
        $logger->info("[Pass] - admin/routes validation is passed");

    }

    /**
     * @Then /^admin get request with valid access token$/
     */
    public function adminGetRequestWithValidAccessToken()
    {
        $logger = Logger::getLogger("EDUMATE ADMIN API GET REQUEST");
        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $logger->info($common::$bluevalleyURL);
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $bear_token"
                    ]
                ]
            );
            $common::$response = $common::$response->getBody()->read(2048);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

    /**
     * @Then /^validate call data detail$/
     */
    public function validateCallDataDetail()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ADMIN CALL API RESPONSE DATA VALIDATION");
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        #$dataData = $data['data'];
        $logger->info("[Record] - response body data detail: " . json_encode($data));
        $limitDailyCall = $data['limit_daily_calls'];
        $dailyCalled = $data['daily_called'];
        $calls = $data['calls'];
        // validation
        PHPUnit\Framework\Assert::assertEquals(null , $limitDailyCall);
        $logger->info("[Pass] - limited daily call validation is passed");
        PHPUnit\Framework\Assert::assertEquals("integer", gettype($dailyCalled));
        $logger->info("[Pass] - daily called validation is passed");
        PHPUnit\Framework\Assert::assertEquals("array", gettype($calls));
        $logger->info("[Pass] - calls validation is passed");
    }

    /**
     * @Then /^validate routes data detail$/
     */
    public function validateRoutesDataDetail()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ADMIN ROUTES API RESPONSE DATA VALIDATION");
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        #$dataData = $data['data'];
        $logger->info("[Record] - response body data detail: " . json_encode($data));
        if ($data != []) {
            if (array_key_exists('learn', $data)) {
                $learn = $data['learn'];
                $meeting = $data['meeting'];
                // validation
                PHPUnit\Framework\Assert::assertEquals(["task_details", "task_resources"], $learn);
                $logger->info("[Pass] - data learn validation is passed");
                PHPUnit\Framework\Assert::assertEquals("array", gettype($meeting));
                $logger->info("[Pass] - meeting validation is passed");
            }elseif (array_key_exists('person', $data)){
                $person = $data['person'];
                PHPUnit\Framework\Assert::assertEquals(["carer_details", "contact_details", "student_details"], $person);
            }else{
                $logger->info("[Record] -  no 'learning', 'meeting' and 'person' in 'data'");
            }
        }else{
            $logger->info("[Pass] - no data of 'learn' and 'meeting'");
        }
    }
}