<?php
use Behat\Behat\Context\Context;

/**
 * Created by PhpStorm.
 * User: admun
 * Date: 11/27/2019
 * Time: 4:47 PM
 */
class CommonSteps implements Context
{

    public static $response;
    public static $broadcastApi;
    public static $toke;
    public static $uri;
    public static $bluevalleyURL;
    public static $bear_token;
    public static $err_missing_client_id_or_secret;

        /**
     * @Given /^username "([^"]*)" for the login request "([^"]*)"$/
     */
    public function usernameAndPasswordForTheRequest($username, $url)
    {
        //echo "Number 1";
        //echo $url."---".$username." -------".$password;
        $config = new Config();
        $password = $config::$password;
        $ur = $config::$url;
        $client = new GuzzleHttp\Client(['verify' => false ]);
        self::$response = $client->request('POST', $ur.$url, [
            'form_params' => array('username'=>$username,'password'=>$password)]);

    }

    /**
     * @Then /^verify request generates token$/
     */
    public function verifyRequestGeneratesToken()
    {
        $logger = Logger::getLogger("API");
        $code = self::$response->getStatusCode();
        //echo "Status code is - ".$code;
        $res = json_decode(self::$response->getBody(), true);
//        //echo "toke value is - ".$res['token'];
        self::$toke = $res['token'];
        $logger->info("Generated token - " . self::$toke);
    }

    /**
    * @Given /^uri "([^"]*)"$/
    */
    public function uri($arg1)
    {
        $config = new Config();
        self::$uri = $config::$bluevalley_url.$arg1;
        echo self::$uri;
    }

    /**
     * @Given /^bluevalley request url "([^"]*)"$/
     */
    public function bluevalleyRequestURL($arg1)
    {
        $config = new Config();
        self::$bluevalleyURL = $config::$bluevalley_url.$arg1;
        $logger = Logger::getLogger("API");
        $logger->info(self::$bluevalleyURL );
        echo self::$bluevalleyURL;
    }

    /**
     * @Given /^scenario name "([^"]*)"$/
     */
    public function scenarioName($arg1)
    {
        $logger = Logger::getLogger("API");
        $logger->info("scenario - " . $arg1 );
    }

    /**
     * @Then /^validate the status code "([^"]*)"$/
     */
    public function validateTheStatusCode($arg1)
    {
        $logger = Logger::getLogger("API");
        self::$response->getStatusCode();
        $logger->info("Status code is - " . self::$response->getStatusCode());
    }

    /**
     * @Then /^validate bear token$/
     */
    public function validateBearToken()
    {
        $logger = Logger::getLogger("VALIDATE BEAR TOKEN");
        $logger->info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        $logger->info(self::$response);
        $res = json_decode(self::$response, true);
        $logger->info($res);
        $data = $res['data'];
        $logger->info($data);
        #$dataData = $data['data'];
        $logger->info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        $logger->info($data);
        $access_token = $data['access_token'];
        self::$bear_token = $access_token;
        $dataData = json_encode($data,true);
        #$logger->info($dataData);
        #$logger->info($data['pagination']);
        // validation
        PHPUnit\Framework\Assert::assertEquals("string", gettype(self::$bear_token));
        $logger->info("[PASS] - Get a valid access token");

    }

    /**
     * @Then /^Get the error description of missing client_id or client_secret
     */
    public function getErrorDescriptionOfMissingClientIdOrClientSecret()
    {
        $config = new Config();
        $logger = Logger::getLogger("GET THE error description of missing client_id or client_secret");
        $key = "error_description";
        $err_missing_client_id_or_secret = $config::$error_missing_client_id_or_secret;
        self::$err_missing_client_id_or_secret = $err_missing_client_id_or_secret[$key];
        $logger->info("ggggggggggg" .$error_missing_client_id_or_secret);

    }

    /**
     * @Given /^bluevalley request url  with (.*)$/
     */
    public function bluevalleyRequestUrlWith($invalid_url)
    {
        $config = new Config();
        self::$bluevalleyURL = $config::$bluevalley_url.$invalid_url;

    }


    /**
     * @Then /^(.*) get request with invalid access token$/
     */
    public function getRequestWithInvalidAccessToken($api)
    {
        $logger = Logger::getLogger("EDUMATE ". $api ." API");

        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        $invalid_bear_token = $bear_token ."invalid";
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $invalid_bear_token"
                    ]
                ]
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

    /**
     * @Then /^(.*) get request with invalid url$/
     */
    public function getRequestWithInvalidUrl($api)
    {
        $logger = Logger::getLogger("EDUMATE ".$api. " API GET REQUEST");
        $common = new CommonSteps();
        $client = new GuzzleHttp\Client(['verify' => false]);
        $logger->info($common::$bluevalleyURL);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

}