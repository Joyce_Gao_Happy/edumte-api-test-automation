<?php

use Behat\Behat\Tester\Exception\PendingException;
#use Behat\Behat\Context\Context;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Request;
require_once __DIR__ . '/Config.php';
Logger::configure('config.xml');

class EdumateAPIContactSteps implements Context
{
    /**
     * @Given /^bluevalley request url "([^"]*)" with (.*)$/
     */
    public function bluevalleyRequestUrlWith($contactURL, $contactId)
    {
        $common = new CommonSteps();
        $config = new Config();
        $common::$bluevalleyURL = $config::$bluevalley_url.$contactURL.$contactId;
    }

    /**
     * @Then /^contact get request with valid access token$/
     */
    public function contactGetRequestWithValidAccessToken()
    {
        $logger = Logger::getLogger("EDUMATE CONTACT API GET REQUEST");
        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        $logger->info($bear_token);
        $logger->info($common::$bluevalleyURL);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $bear_token"
                    ]
                ]
            );
            $common::$response = $common::$response->getBody()->read(2048);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }



}
