<?php
use Behat\Behat\Context\Context;

/**
 * Created by PhpStorm.
 * User: admun
 * Date: 11/27/2019
 * Time: 5:08 PM
 */
class BroadcastSteps implements Context
{

    /**
     * @When /^startdate (.*) , enddate (.*) , limit (.*) and offset (.*)$/
     */
    public function startdateEnddateLimitAndOffset($startdate, $enddate, $limit, $offset)
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("API");
        $logger->info("[Record] -".$startdate." - ".$enddate." - ".$limit." - ".$offset);
        $tokenGenerated = $common::$toke;
        $client = new GuzzleHttp\Client(['verify' => false ]);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$uri."dateStart=".$startdate."&dateEnd=".$enddate."&limit=".$limit."&offset=".$offset,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $tokenGenerated"
                    ]
                ]
            );
        }
        catch(Exception $e) {
            $logger->info("[FAIL] -Error while processing the request - ".substr($e,1,350));
        }


    }

    /**
     * @Then /^validate the photos count$/
     */
    public function validateThePhotosCount()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("API");
        $common::$response->getBody();
        $res = json_decode($common::$response->getBody(), true);
        $photos = $res[0]['photos'];
        if (count($photos) == 0) {
            $logger->info("[FAIL] - Photos count is - 0");
            PHPUnit\Framework\Assert::assertEquals('Photos are null', 'Photos should not be null');
        }
        $logger->info("[PASS] - Photos count is - " .count($photos));
    }
}