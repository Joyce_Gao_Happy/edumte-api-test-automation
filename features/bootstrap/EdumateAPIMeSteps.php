<?php
use Behat\Behat\Tester\Exception\PendingException;
#use Behat\Behat\Context\Context;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Request;
require_once __DIR__ . '/Config.php';
Logger::configure('config.xml');


class EdumateAPIMeSteps implements Context
{

    /**
     * @Then me get request with valid access token
     */
    public function meGetRequestWithValidAccessToken()
    {
        $logger = Logger::getLogger("API");
        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $bear_token"
                    ]
                ]
            );
            $common::$response = $common::$response->getBody()->read(20480);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }

    }

    /**
     * @Then /^validate me data response with (.*) and (.*) and (.*) and (.*)$/
     */
    public function validateMeDataResponseWithAndAndAnd($contactId, $firstname, $surname, $emailAddress)
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ME API RESPONSE DATA VALIDATION");
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        #$dataData = $data['data'];
        $logger->info("[Record] - response body data detail: " . json_encode($data));
        $contactIdValue = $data['contactId'];
        $firstnameValue = $data['firstname'];
        $surnameValue = $data['surname'];
        $emailAddressValue = $data['emailAddress'];
        // validation
        PHPUnit\Framework\Assert::assertEquals($contactId, $contactIdValue);
        $logger->info("[PASS] - contact id validation is passed");
        PHPUnit\Framework\Assert::assertEquals($firstname, $firstnameValue);
        $logger->info("[PASS] - first name validation is passed");
        PHPUnit\Framework\Assert::assertEquals($surname,$surnameValue);
        $logger->info("[PASS] - surname validation is passed");
        PHPUnit\Framework\Assert::assertEquals($emailAddress, $emailAddressValue);
        $logger->info("[PASS] - email address validation is passed");

    }

    /**
     * @Then /^validate data error message of invalid token$/
     */
    public function validateDataErrorMessageOfInvalidToken()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE ME API RESPONSE DATA VALIDATION");
        //$common::$response->getBody();
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $error_data = $res["data"];
        $logger->info("[Record] - response body error data detail: " . json_encode($error_data));
        $error_description = $error_data['error_description'];
        $error = $error_data['error'];
        $logger->info($error);
        //$error_description = $error_data['error_description'];
        // validation
        PHPUnit\Framework\Assert::assertEquals("unauthorized_client", $error);
        $logger->info("[PASS] - error validation is passed");
        PHPUnit\Framework\Assert::assertEquals("Unable to verify the given JWT through the given configuration. If the \"lexik_jwt_authentication.encoder\" encryption options have been changed since your last authentication, please renew the token. If the problem persists, verify that the configured keys/passphrase are valid.", $error_description);
        $logger->info("[PASS] - error description validaton is passed");
    }

    /**
     * @Then /^me get request with invalid access token$/
     */
    public function meGetRequestWithInvalidAccessToken()
    {
        $logger = Logger::getLogger("EDUMATE ME API");

        $common = new CommonSteps();
        $logger->info("[Record] - " . $common::$bear_token );
        $client = new GuzzleHttp\Client(['verify' => false]);
        $bear_token = strval($common::$bear_token);
        $invalid_bear_token = $bear_token ."invalid";
        try {
            $common::$response = $client->request(
                'GET',
                $common::$bluevalleyURL,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $invalid_bear_token"
                    ]
                ]
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

}