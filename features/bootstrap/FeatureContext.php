<?php
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Request;
require_once __DIR__ . '/Config.php';
Logger::configure('config.xml');
/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext
{

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected static $response;
    protected static $broadcastApi;
    protected static $toke;

    /**
     * @BeforeSuite
     */
    public static function beforeSuite()
    {
        $logger = Logger::getLogger("API");
        $logger->info("------------");
    }

    /**
     * @AfterSuite
     */
    public static function afterSuite()
    {
        shell_exec("java -jar reports_mac.jar");
    }

    /**
     * @AfterScenario
     */
    public function afterScenario(AfterScenarioScope $scope)
    {
        $logger = Logger::getLogger("API");
        $logger->info("------Ends------");
    }


    /**
     * @Then /^authorize post request with body client Id (.*) , client secret (.*) , refresh token (.*)$/
     */
    public function sendAuthorizePostRequestWithClientIdAndClientSecretAndRefreshToken($clientId, $clientSecret, $refreshToken)
    {
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API POST request");
        $common = new CommonSteps();
        $client = new GuzzleHttp\Client(['verify' => false]);
        $logger->info("[Record] - Authorize - request - URL: " . $common::$bluevalleyURL);
        $logger->info("[Record] - Authorize - Post - client_id: " . $clientId . " - client_secret: " . $clientSecret . " - refresh_token: " . $refreshToken);
        try {
            $common::$response = $client->request(
                'POST',
                $common::$bluevalleyURL,
                ['form_params' => array('client_id' => $clientId, 'client_secret' => $clientSecret, 'refresh_token' => $refreshToken)
                ]
            );
            $common::$response = $common::$response->getBody()->read(2048);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }

    }

    /**
     * @Then /^authorize post request with client Id (.*) , client secret (.*)$/
     */
    public function sendAuthorizePostRequestWithClientIdAndClientSecret($clientId, $clientSecret)
    {
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API POST request");
        $common = new CommonSteps();
        $client = new GuzzleHttp\Client(['verify' => false]);
        $logger->info("[Record] - Authorize - request - URL: " . $common::$bluevalleyURL);
        $logger->info("[Record] - Authorize - Post - client_id: " . $clientId . " - client_secret: " . $clientSecret);
        try {
            $common::$response = $client->request(
                'POST',
                $common::$bluevalleyURL,
                ['form_params' => array('client_id' => $clientId, 'client_secret' => $clientSecret)
                ]
            );
            $common::$response = $common::$response->getBody()->read(2048);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }

    }


    /**
     * @Then /^authorize post request without client Id or client secret (.*)$/
     */
    public function sendAuthorizePostRequestWithoutClientIdOrClientSecret($clientArg)
    {
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API POST REQUEST");
        $common = new CommonSteps();
        $client = new GuzzleHttp\Client(['verify' => false]);
        $logger->info("[Record] - Authorize - request - URL: " . $common::$bluevalleyURL);
        $logger->info("[Record] - Authorize - Post - client_secret: " . $clientArg);
        try {
            //$common::$response = $client->post($common::$bluevalleyURL);
            if (strlen(strval($clientArg)) == 32) {
                $common::$response = $client->request(
                    'POST',
                    $common::$bluevalleyURL,
                    ['form_params' => array('client_id' => $clientArg)
                    ]
                );
            } elseif (strlen(strval($clientArg)) == 64) {
                $common::$response = $client->request(
                    'POST',
                    $common::$bluevalleyURL,
                    ['form_params' => array('client_secret' => $clientArg)
                    ]
                );
            } else {
                $logger->info("The argument client id or client secret isn't valid");
                exit();
            }
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
        //    $logger->info("[FAIL] -Error while processing the request - " . substr($e, 1, 350));
        //    $logger->info( Psr7\str($e->getRequest()) );
        //$logger->info( Psr7\str($e->getResponse()) );
        //} catch (\GuzzleHttp\Exception\GuzzleException $e) {
        //    $logger->info($e);
        //}

    }

    /**
     * @Then validate success as true
     */
    public function validateSuccessAsTrue()
    {
        $common = new CommonSteps();

        $logger = Logger::getLogger("EDUMATE API KEY SUCCESS VALIDATION");
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $success = $res['success'];
        //validation
        PHPUnit\Framework\Assert::assertEquals(true, $success);
        $logger->info("[Pass] - Authorize response body value of success is true");

    }


    /**
     * @Then validate data keys
     */
    public function validateDataKeys()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API RESPONSE DATA VALIDATION");
        #$common::$response->getBody();
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $data = $res['data'];
        #$dataData = $data['data'];
        $logger->info("[Record] - response body data detail: " . json_encode($data));
        $access_token = $data['access_token'];
        $token_type = $data['token_type'];
        $expires_in = $data['expires_in'];
        // validation
        PHPUnit\Framework\Assert::assertEquals("string", gettype($access_token));
        $logger->info("[PASS] - access token validation is passed");
        PHPUnit\Framework\Assert::assertEquals("Bearer", $token_type);
        $logger->info("[PASS] - token type is Bearer");
        PHPUnit\Framework\Assert::assertEquals("integer", gettype($expires_in));
        $logger->info("[PASS] - expires_in validation is passed");
        if (in_array('refresh_token', $data)) {
            PHPUnit\Framework\Assert::assertEquals("string", gettype($data['refresh_token']));
            $logger->info("[PASS] - refresh token validation is passed");
        } else {
            $logger->info("[PASS] - No refresh token in response");
        }
    }

    /**
     * @Then validate success as false
     */
    public function validateSuccessAsFalse()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE API KEY SUCCESS VALIDATION");
        //$common::$response->getBody();
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $success = $res["success"];
        //validation
        $logger->info("The success value is " . $success);
        PHPUnit\Framework\Assert::assertEquals(false, $success);
        $logger->info("[Pass] - Authorize response body value of success is false");
    }

    /**
     * @Then validate data error message of missing client id  or client secret
     */
    public function validateDataErrorMessageClientIdAndClientSecret()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API RESPONSE DATA VALIDATION");
        //$common::$response->getBody();
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $error_data = $res["data"];
        $logger->info("[Record] - response body error data detail: " . json_encode($error_data));
        $error_description = $error_data['error_description'];
        $error = $error_data['error'];
        $logger->info($error);
        //$error_description = $error_data['error_description'];
        // validation
        PHPUnit\Framework\Assert::assertEquals("invalid_request", $error);
        $logger->info("[PASS] - error validation is passed");
        $logger->info( $common::$err_missing_client_id_or_secret);
        $logger->info("xxxxxxxxxxxxxxxxxxxxxx");
        $logger->info($common::$bluevalleyURL);
        PHPUnit\Framework\Assert::assertEquals("Request missing 'client_id' and/or 'client_secret' parameters.", $error_description);
        $logger->info("[PASS] - error description validaton is passed");

    }


    /**
     * @Then /^authorize post request body with invalid client Id (.*) or client secret (.*)$/
     */
    public function authorizePostRequestBodyWithInvalidClientIdOrClientSecret($clientId, $clientSecret)
    {
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API POST REQUEST");
        $common = new CommonSteps();
        $client = new GuzzleHttp\Client(['verify' => false]);
        $logger->info("[Record] - Authorize - request - URL: " . $common::$bluevalleyURL);
        $logger->info("[Record] - Authorize - Post - client_secret: " . $clientSecret . " client_id: " . $clientId);
        try {
            //$common::$response = $client->post($common::$bluevalleyURL);
            $common::$response = $client->request(
                'POST',
                $common::$bluevalleyURL,
                ['form_params' => array('client_id' => $clientId, 'client_secret' => $clientSecret)
                ]
            );
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $logger->info($e->getResponse()->getBody());
            $common::$response = $e->getResponse()->getBody();
            return $common::$response;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $logger->info("Guzzle exception" . $e);
        }
    }

    /**
     * @Then /^validate data error message of invalid client id or client secret$/
     */
    public function validateDataErrorMessageOfInvalidClientIdOrClientSecret()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API RESPONSE DATA VALIDATION");
        //$common::$response->getBody();
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $error_data = $res["data"];
        $logger->info("[Record] - response body error data detail: " . json_encode($error_data));
        $error_description = $error_data['error_description'];
        $error = $error_data['error'];
        $logger->info($error);
        //$error_description = $error_data['error_description'];
        // validation
        PHPUnit\Framework\Assert::assertEquals("invalid_client", $error);
        $logger->info("[PASS] - error validation is passed");
        PHPUnit\Framework\Assert::assertContains("Invalid", $error_description);
        $logger->info("[PASS] - error description validaton is passed");
    }

    /**
     * @Then /^validate data error message of resource not found$/
     */
    public function validateDataErrorMessageOfResourceNotFound()
    {
        $common = new CommonSteps();
        $logger = Logger::getLogger("EDUMATE AUTHORIZE API RESPONSE DATA VALIDATION");
        //$common::$response->getBody();
        $logger->info($common::$response);
        $res = json_decode($common::$response, true);
        $logger->info($res);
        $error_data = $res["data"];
        $logger->info("[Record] - response body error data detail: " . json_encode($error_data));
        $error_description = $error_data['error_description'];
        $error = $error_data['error'];
        $logger->info($error);
        //$error_description = $error_data['error_description'];
        // validation
        PHPUnit\Framework\Assert::assertEquals("not_found", $error);
        $logger->info("[PASS] - error validation is passed");
        PHPUnit\Framework\Assert::assertContains("We could not find the resource you requested.", $error_description);
        $logger->info("[PASS] - error description validaton is passed");
    }


}
