<?php
use Behat\Behat\Context\Context;

/**
 * Created by PhpStorm.
 * User: admun
 * Date: 11/27/2019
 * Time: 4:54 PM
 */
class AbsentManagementSteps implements Context
{

    /**
     * @Then /^mark absent for the studentid (.*) , absencereasonId (.*) , abscencedetails (.*) and info (.*)$/
     */
    public function markAbsentForTheStudentidAbsencereasonIdAbscencedetailsAndInfo($clientId, $absenceReasonId, $absenceDetails, $info)
    {
        $logger = Logger::getLogger("API");
        $logger->info("[Record] -".$clientId." - ".$absenceReasonId." - ".$absenceDetails." - ".$info);
        $common = new CommonSteps();
        $tokenGenerated = $common::$toke;;
        $client = new GuzzleHttp\Client(['verify' => false ]);
        try {
            $common::$response = $client->request(
                'POST',
                $common::$url,
                ['headers' =>
                    [
                        'Authorization' => "Bearer $tokenGenerated"
                    ],'form_params' => array('studentId'=>$clientId,'absenceReasonId'=>$absenceReasonId,'absenceDetails'=>$absenceDetails,'absenceInfo[]'=>$info)
                ]
            );
        }
        catch(Exception $e) {
            $logger->info("[FAIL] -Error while processing the request - ".substr($e,1,350));
            PHPUnit\Framework\Assert::assertEquals(true, "Error processing the request");
        }

    }

    /**
     * @Then /^xxvalidate the absent record$/
     */
    public function validateTheMessage()
    {
        $common = new CommonSteps();

        $logger = Logger::getLogger("API");
        $common::$response->getBody();
        $res = json_decode($common::$response->getBody(), true);
//        //echo "Json response are - ".implode( ", ", $res );;
        $success = $res['success'];
//        //echo "success response are - ".$success;
        if($success == 1){
            $logger->info("[PASS] - Absent marked");
        }else{
            $logger->info("[FAIL] - Absent not marked".$res['message']);
            PHPUnit\Framework\Assert::assertEquals(true, $success);
            PHPUnit\Framework\Assert::assertEquals("Reason saved and verified", $res['message']);
        }


    }
}