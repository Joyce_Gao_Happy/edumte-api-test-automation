
## Installation 
- composer install

## Running your tests
- To run a tags , behat --tags absent
- To run a feature file , behat features/Broadcast.feature
- To run all the feature file and generate the report , behat -c behat.yml

## In Mac
- To run a tags , bin/behat --tags absent
- To run a feature file , bin/behat features/Broadcast.feature
- To run all the feature file and generate the report , bin/behat -c behat.yml

